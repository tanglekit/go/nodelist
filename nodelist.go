package nodelist

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"

	"gitlab.com/tanglekit/go/logs"

	"github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/trinary"
)

const (
	checkHealthyNodesTickerInterval   = time.Duration(1) * time.Minute
	checkUnhealthyNodesTickerInterval = time.Duration(1) * time.Minute
)

var (
	ErrTimeout = errors.New("Request timeout")
)

type JSON_NodesIotaWorks struct {
	Host string `json:"node"`
}

type Milestone struct {
	Hash      *trinary.Hash
	Index     int64
	Timestamp uint64
}

func getJSON(url string, target interface{}) error {
	var myClient = &http.Client{Timeout: 10 * time.Second}

	resp, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(result, target)
	return err
}

func GetNodeList(addresses []string) (map[string]*struct{}, error) {
	nodeList := make(map[string]*struct{})

	for _, address := range addresses {
		if strings.Contains(address, "nodes.iota.works/api/live/ssl") {
			var nodeListFromJSON []JSON_NodesIotaWorks
			err := getJSON(address, &nodeListFromJSON)
			if err != nil {
				return nil, fmt.Errorf("Couldn't load nodes from json '%s' (%v)", address, err)
			}
			for _, node := range nodeListFromJSON {
				nodeList[node.Host] = nil
			}
		} else {
			if strings.HasSuffix(address, ".json") {
				var nodeListFromJSON []string
				err := getJSON(address, &nodeListFromJSON)
				if err != nil {
					return nil, fmt.Errorf("Couldn't load nodes from json '%s' (%v)", address, err)
				}
				for _, node := range nodeListFromJSON {
					nodeList[node] = nil
				}
			} else {
				nodeList[address] = nil
			}
		}
	}
	return nodeList, nil
}

// PendingApiRequest contains the information about a request and the response channel for signaling
type PendingApiRequest struct {
	apiRequest        api.IRICommand
	apiRequestParam   interface{}
	apiRequestParam2  interface{}
	apiRequestParam3  interface{}
	apiRequestParam4  interface{}
	apiResponseChan   chan interface{}
	errorChan         chan *error
	signalChanTimeout chan bool
}

func (pr *PendingApiRequest) CheckTimeoutSignal() (timedOut bool) {
	select {
	case _, _ = <-pr.signalChanTimeout:
		close(pr.signalChanTimeout)
		return true
	default:
		return false
	}
}

type ApiNode struct {
	Address        string
	Version        string
	API            *api.API
	APICheckHealth *api.API
}

type ApiNodeList struct {
	apiNodesAddresses        map[string]*struct{}
	apiNodesHealthy          chan *ApiNode
	apiNodesUnhealthy        chan *ApiNode
	apiNodesLock             *sync.Mutex
	apiNodesHealthyCount     int
	validVersions            []string
	milestoneTolerance       int64
	latestMilestoneLock      *sync.Mutex
	latestSolidMilestoneLock *sync.Mutex
	latestMilestone          *Milestone
	latestSolidMilestone     *Milestone
	nodeRNG                  *rand.Rand
	timeout                  time.Duration
	pendingApiRequests       chan *PendingApiRequest
	checkHealthyNodesQuit    chan struct{}
	checkUnhealthyNodesQuit  chan struct{}
	killWorkerSignal         chan struct{}
	startNodeWorkers         bool
}

func (nl *ApiNodeList) GetTotalNodeCount() int {
	return len(nl.apiNodesAddresses)
}

func (nl *ApiNodeList) GetHealthyNodeCount() int {
	return nl.apiNodesHealthyCount
}

func (nl *ApiNodeList) HandleResponse(apiNode *ApiNode, pr *PendingApiRequest, resp interface{}, err error) (quit bool) {
	timedOut := pr.CheckTimeoutSignal()

	if !timedOut {
		if err != nil {
			pr.errorChan <- &err
		} else {
			pr.apiResponseChan <- resp
		}
	} else {
		err = ErrTimeout
	}

	return nl.HandleError(apiNode, err)
}

func (nl *ApiNodeList) HandleRequests() {
	for {
		select {
		case <-nl.killWorkerSignal:
			// Kill a single worker, or all if channel is closed
			return
		case pr := <-nl.pendingApiRequests:
			select {

			case _, _ = <-pr.signalChanTimeout:

				err := errors.New("No healty node found")
				pr.errorChan <- &err
				close(pr.signalChanTimeout)
				continue

			case apiNode := <-nl.apiNodesHealthy:

				switch pr.apiRequest {

				case api.GetNodeInfoCmd:
					resp, err := apiNode.API.GetNodeInfo()
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.CheckConsistencyCmd:
					resp, resp2, err := apiNode.API.CheckConsistency(pr.apiRequestParam.([]trinary.Hash)...)
					nl.HandleResponse(apiNode, pr, api.CheckConsistencyResponse{State: resp, Info: resp2}, err)

				case api.FindTransactionsCmd:
					resp, err := apiNode.API.FindTransactions(pr.apiRequestParam.(api.FindTransactionsQuery))
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.GetTrytesCmd:
					resp, err := apiNode.API.GetTrytes(pr.apiRequestParam.([]trinary.Hash)...)
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.GetInclusionStatesCmd:
					resp, err := apiNode.API.GetInclusionStates(pr.apiRequestParam.(trinary.Hashes))
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.GetBalancesCmd:
					resp, err := apiNode.API.GetBalances(pr.apiRequestParam.(trinary.Hashes))
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.GetTransactionsToApproveCmd:
					resp, err := apiNode.API.GetTransactionsToApprove(0, pr.apiRequestParam.([]trinary.Hash)...)
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.AttachToTangleCmd:
					resp, err := apiNode.API.AttachToTangle(pr.apiRequestParam.(trinary.Hash), pr.apiRequestParam2.(trinary.Hash), pr.apiRequestParam3.(uint64), pr.apiRequestParam4.([]trinary.Trytes))
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.BroadcastTransactionsCmd:
					resp, err := apiNode.API.BroadcastTransactions(pr.apiRequestParam.([]trinary.Trytes)...)
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.StoreTransactionsCmd:
					resp, err := apiNode.API.StoreTransactions(pr.apiRequestParam.([]trinary.Trytes)...)
					nl.HandleResponse(apiNode, pr, resp, err)

				case api.WereAddressesSpentFromCmd:
					resp, err := apiNode.API.WereAddressesSpentFrom(pr.apiRequestParam.([]trinary.Hash)...)
					nl.HandleResponse(apiNode, pr, resp, err)
				}
			}
		}
	}
}

func (nl *ApiNodeList) HandleError(apiNode *ApiNode, err error) (quit bool) {

	if err != nil {
		nl.apiNodesUnhealthy <- apiNode
		nl.apiNodesHealthyCount--
		logs.Log.Warningf("Removed unhealty node: %v, Err: %v", apiNode.Address, err.Error())

		if nl.startNodeWorkers {
			nl.killWorkerSignal <- struct{}{}
		}
		return true
	}

	nl.apiNodesHealthy <- apiNode
	return false
}

func (nl *ApiNodeList) checkNodesWorker(interval time.Duration, quitChan chan struct{}, apiNodesChan chan *ApiNode, wasHealthy bool) {

	checkNodesTicker := time.NewTicker(interval)
	for {
		select {
		case <-quitChan:
			return

		case <-checkNodesTicker.C:
			nl.checkNodes(apiNodesChan, false, false, wasHealthy)
		}
	}
}

func (nl *ApiNodeList) checkNodes(apiNodesChan chan *ApiNode, printStatus bool, firstCheck bool, wasHealthy bool) {

	seenNodes := make(map[string]*struct{})
	checkNodesWaitGroup := &sync.WaitGroup{}

forLoop:
	for {
		select {
		case apiNode := <-apiNodesChan:

			_, seenAlready := seenNodes[apiNode.Address]
			if seenAlready {
				// We are back at the start of the queue => quit loop
				// Re-add it to the queue
				apiNodesChan <- apiNode
				break forLoop
			}
			seenNodes[apiNode.Address] = nil // mark Node as seen

			checkNodesWaitGroup.Add(1)

			go func(apiNode *ApiNode, wg *sync.WaitGroup) {
				retry, err := nl.checkNodeHealth(apiNode, firstCheck)
				if err != nil {
					if wasHealthy {
						nl.apiNodesHealthyCount--
						if nl.startNodeWorkers {
							nl.killWorkerSignal <- struct{}{}
						}
					}
					if printStatus {
						logs.Log.Infof("Node %v is NOT healthy. Err: %v", apiNode.Address, err)
					}
					if retry {
						// Node was unhealthy, but maybe not permanent (version correct etc.)
						nl.apiNodesUnhealthy <- apiNode
					}
				} else {
					if printStatus {
						logs.Log.Infof("Node %v is healthy", apiNode.Address)
					}
					nl.apiNodesHealthy <- apiNode
					if !wasHealthy {
						nl.apiNodesHealthyCount++
						if nl.startNodeWorkers {
							go nl.HandleRequests() // Start a new worker for the healthy node
						}
					}
				}

				wg.Done()
				return

			}(apiNode, checkNodesWaitGroup)

		default:
			// Non blocking read is essential here, otherwise it causes a deadlock if the queue is empty
			break forLoop
		}
	}

	checkNodesWaitGroup.Wait()
}

func (nl *ApiNodeList) Init(nodes []string, timeout time.Duration, validVersions []string, milestoneTolerance int64, startNodeWorkers bool) error {
	var err error

	nl.apiNodesAddresses, err = GetNodeList(nodes)
	if err != nil {
		return err
	}

	nl.apiNodesHealthy = make(chan *ApiNode, len(nl.apiNodesAddresses))
	nl.apiNodesUnhealthy = make(chan *ApiNode, len(nl.apiNodesAddresses))
	nl.apiNodesLock = &sync.Mutex{}
	nl.apiNodesHealthyCount = 0
	nl.validVersions = validVersions
	nl.milestoneTolerance = milestoneTolerance
	nl.latestMilestoneLock = &sync.Mutex{}
	nl.latestSolidMilestoneLock = &sync.Mutex{}
	nl.latestMilestone = nil
	nl.latestSolidMilestone = nil
	nl.nodeRNG = rand.New(rand.NewSource(time.Now().Unix()))
	nl.timeout = timeout
	nl.pendingApiRequests = make(chan *PendingApiRequest, 50) // TODO Limit
	nl.checkHealthyNodesQuit = make(chan struct{})
	nl.checkUnhealthyNodesQuit = make(chan struct{})
	nl.killWorkerSignal = make(chan struct{}, len(nl.apiNodesAddresses))
	nl.startNodeWorkers = startNodeWorkers

	for address := range nl.apiNodesAddresses {
		apiNode, err := api.ComposeAPI(api.HTTPClientSettings{URI: address, Client: &http.Client{Timeout: timeout}})
		if err != nil {
			continue
		}
		apiNodeCheckHealth, err := api.ComposeAPI(api.HTTPClientSettings{URI: address, Client: &http.Client{Timeout: 2 * time.Second}})
		if err != nil {
			continue
		}
		nl.apiNodesUnhealthy <- &ApiNode{address, "", apiNode, apiNodeCheckHealth}
	}

	nl.checkNodes(nl.apiNodesUnhealthy, true, true, false)

	if len(nl.apiNodesHealthy) == 0 {
		return errors.New("No healty IRI node found")
	}

	go nl.checkNodesWorker(checkUnhealthyNodesTickerInterval, nl.checkUnhealthyNodesQuit, nl.apiNodesUnhealthy, false)
	go nl.checkNodesWorker(checkHealthyNodesTickerInterval, nl.checkHealthyNodesQuit, nl.apiNodesHealthy, true)

	logs.Log.Infof("Successfully added %v/%v nodes", len(nl.apiNodesHealthy), len(nl.apiNodesAddresses))

	return nil
}

func (nl *ApiNodeList) Close() {
	close(nl.checkHealthyNodesQuit)
	close(nl.checkUnhealthyNodesQuit)
	close(nl.killWorkerSignal)
}

// GetLatestMilestone returns the latest milestone.
func (nl *ApiNodeList) GetLatestMilestone() (milestone *Milestone) {
	nl.latestMilestoneLock.Lock()
	defer nl.latestMilestoneLock.Unlock()

	return nl.latestMilestone
}

// GetLatestSolidMilestone returns the latest solid milestone.
func (nl *ApiNodeList) GetLatestSolidMilestone() (milestone *Milestone) {
	nl.latestSolidMilestoneLock.Lock()
	defer nl.latestSolidMilestoneLock.Unlock()

	return nl.latestSolidMilestone
}

func (nl *ApiNodeList) ExecuteFunc(signalChanTimeout chan bool, signalChanQuit chan struct{}, fn func(*ApiNode) error, maxRetries int) error {
	defer close(signalChanTimeout)

	select {
	case <-signalChanTimeout:
		// No healthy node found in time
		return errors.New("No healty node found")

	case <-signalChanQuit:
		// Global quit signal was sent
		return nil

	case apiNode := <-nl.apiNodesHealthy:
		// Healthy node found
		go func() {
			err := fn(apiNode)
			if nl.HandleError(apiNode, err) && maxRetries > 0 {
				// There was an error => Retry
				newSignalChanTimeout := make(chan bool, 1)
				nl.ExecuteFunc(newSignalChanTimeout, signalChanQuit, fn, maxRetries-1)
			}
		}()
		return nil
	}
}

func (nl *ApiNodeList) checkNodeHealth(apiNode *ApiNode, firstCheck bool) (retry bool, err error) {
	nodeInfoResp, err := apiNode.APICheckHealth.GetNodeInfo()
	if err != nil {
		return !firstCheck, err
	}

	nl.latestMilestoneLock.Lock()
	if (nl.latestMilestone == nil) || (nl.latestMilestone.Index < nodeInfoResp.LatestMilestoneIndex) {
		milestoneTxs, err := apiNode.API.GetTransactionObjects(nodeInfoResp.LatestMilestone)
		if err == nil {
			nl.latestMilestone = &Milestone{Hash: &nodeInfoResp.LatestMilestone, Index: nodeInfoResp.LatestMilestoneIndex, Timestamp: milestoneTxs[0].Timestamp}
		}
	}
	nl.latestMilestoneLock.Unlock()

	nl.latestSolidMilestoneLock.Lock()
	if (nl.latestSolidMilestone == nil) || (nl.latestSolidMilestone.Index < nodeInfoResp.LatestSolidSubtangleMilestoneIndex) {
		milestoneTxs, err := apiNode.API.GetTransactionObjects(nodeInfoResp.LatestSolidSubtangleMilestone)
		if err == nil {
			nl.latestSolidMilestone = &Milestone{Hash: &nodeInfoResp.LatestSolidSubtangleMilestone, Index: nodeInfoResp.LatestSolidSubtangleMilestoneIndex, Timestamp: milestoneTxs[0].Timestamp}
		}
	}
	nl.latestSolidMilestoneLock.Unlock()

	if firstCheck {
		apiNode.Version = nodeInfoResp.AppVersion

		versionValid := false
		if nl.validVersions != nil && len(nl.validVersions) > 0 {
			for _, version := range nl.validVersions {
				if strings.HasPrefix(nodeInfoResp.AppVersion, version) {
					versionValid = true
					break
				}
			}
		} else {
			versionValid = true
		}

		if !versionValid {
			return false, fmt.Errorf("outdated [v%v]", nodeInfoResp.AppVersion)
		}
	}

	nl.latestMilestoneLock.Lock()
	defer nl.latestMilestoneLock.Unlock()

	if (nl.latestMilestone != nil) && (nl.latestMilestone.Index-nodeInfoResp.LatestSolidSubtangleMilestoneIndex) > nl.milestoneTolerance {
		return true, fmt.Errorf("out of sync [%d/%d]", nodeInfoResp.LatestSolidSubtangleMilestoneIndex, nl.latestMilestone.Index)
	}

	return true, nil
}

// GetNodeInfo returns information about the connected node.
func (nl *ApiNodeList) GetNodeInfo() (*api.GetNodeInfoResponse, error) {
	pr := &PendingApiRequest{apiRequest: api.GetNodeInfoCmd, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.(*api.GetNodeInfoResponse), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// CheckConsistency checks if a transaction is consistent or a set of transactions are co-consistent.
//
// Co-consistent transactions and the transactions that they approve (directly or indirectly),
// are not conflicting with each other and the rest of the ledger.
//
// As long as a transaction is consistent, it might be accepted by the network.
// In case a transaction is inconsistent, it will not be accepted and a reattachment
// is required by calling ReplayBundle().
func (nl *ApiNodeList) CheckConsistency(hashes ...trinary.Hash) (bool, string, error) {
	pr := &PendingApiRequest{apiRequest: api.CheckConsistencyCmd, apiRequestParam: hashes, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		resp := apiResponse.(*api.CheckConsistencyResponse)
		return resp.State, resp.Info, nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return false, "", *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return false, "", errors.New("Receive timeout")
	}
}

// FindTransactions searches for transaction hashes.
// It allows to search for transactions by passing a query object with addresses, bundle hashes, tags and/or approvees fields.
// Multiple query fields are supported and FindTransactions returns the intersection of the results.
func (nl *ApiNodeList) FindTransactions(query api.FindTransactionsQuery) (trinary.Hashes, error) {
	pr := &PendingApiRequest{apiRequest: api.FindTransactionsCmd, apiRequestParam: query, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.(trinary.Hashes), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// GetTrytes fetches the transaction trytes given a list of transaction hashes.
func (nl *ApiNodeList) GetTrytes(hashes ...trinary.Hash) ([]trinary.Trytes, error) {
	pr := &PendingApiRequest{apiRequest: api.GetTrytesCmd, apiRequestParam: hashes, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.([]trinary.Trytes), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// GetInclusionStates fetches inclusion states of a given list of transactions.
func (nl *ApiNodeList) GetInclusionStates(txHashes trinary.Hashes) ([]bool, error) {
	pr := &PendingApiRequest{apiRequest: api.GetInclusionStatesCmd, apiRequestParam: txHashes, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.([]bool), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// GetBalances fetches confirmed balances of the given addresses at the latest solid milestone.
func (nl *ApiNodeList) GetBalances(addresses trinary.Hashes) (*api.Balances, error) {
	pr := &PendingApiRequest{apiRequest: api.GetBalancesCmd, apiRequestParam: addresses, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.(*api.Balances), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// GetTransactionsToApprove does the tip selection via the connected node.
//
// Returns a pair of approved transactions which are chosen randomly after validating the transaction trytes,
// the signatures and cross-checking for conflicting transactions.
//
// Tip selection is executed by a Random Walk (RW) starting at random point in the given depth,
// ending up to the pair of selected tips. For more information about tip selection please refer to the
// whitepaper (http://iotatoken.com/IOTA_Whitepaper.pdf).
//
// The reference option allows to select tips in a way that the reference transaction is being approved too.
// This is useful for promoting transactions, for example with PromoteTransaction().
func (nl *ApiNodeList) GetTransactionsToApprove(reference ...trinary.Hash) (*api.TransactionsToApprove, error) {
	pr := &PendingApiRequest{apiRequest: api.GetTransactionsToApproveCmd, apiRequestParam: reference, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.(*api.TransactionsToApprove), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// AttachToTangle performs the Proof-of-Work required to attach a transaction to the Tangle by
// calling the attachToTangle IRI API command. Returns a list of transaction trytes and overwrites the following fields:
//
// Hash, Nonce, AttachmentTimestamp, AttachmentTimestampLowerBound, AttachmentTimestampUpperBound
//
// If a Proof-of-Work function is supplied when composing the API, then that function is used
// instead of using the connected node.
func (nl *ApiNodeList) AttachToTangle(trunkTxHash trinary.Hash, branchTxHash trinary.Hash, mwm uint64, trytes []trinary.Trytes) ([]trinary.Trytes, error) {
	pr := &PendingApiRequest{apiRequest: api.AttachToTangleCmd, apiRequestParam: trunkTxHash, apiRequestParam2: branchTxHash, apiRequestParam3: mwm, apiRequestParam4: trytes, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.([]trinary.Trytes), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// BroadcastTransactions broadcasts a list of attached transaction trytes to the network.
// Tip-selection and Proof-of-Work must be done first by calling
// GetTransactionsToApprove and AttachToTangle or an equivalent attach method.
//
// You may use this method to increase odds of effective transaction propagation.
//
// Persist the transaction trytes in local storage before calling this command for first time, to ensure
// that reattachment is possible, until your bundle has been included.
func (nl *ApiNodeList) BroadcastTransactions(trytes ...trinary.Trytes) ([]trinary.Trytes, error) {
	pr := &PendingApiRequest{apiRequest: api.BroadcastTransactionsCmd, apiRequestParam: trytes, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.([]trinary.Trytes), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// StoreTransactions persists a list of attached transaction trytes in the store of the connected node.
// Tip-selection and Proof-of-Work must be done first by calling GetTransactionsToApprove and
// AttachToTangle or an equivalent attach method.
//
// Persist the transaction trytes in local storage before calling this command, to ensure
// reattachment is possible, until your bundle has been included.
//
// Any transactions stored with this command will eventually be erased as a result of a snapshot.
func (nl *ApiNodeList) StoreTransactions(trytes ...trinary.Trytes) ([]trinary.Trytes, error) {
	pr := &PendingApiRequest{apiRequest: api.StoreTransactionsCmd, apiRequestParam: trytes, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {
	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.([]trinary.Trytes), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}

// WereAddressesSpentFrom checks whether the given addresses were already spent from by
// calling the wereAddressesSpentFrom IRI API command.
func (nl *ApiNodeList) WereAddressesSpentFrom(addresses ...trinary.Hash) ([]bool, error) {
	pr := &PendingApiRequest{apiRequest: api.WereAddressesSpentFromCmd, apiRequestParam: addresses, apiResponseChan: make(chan interface{}, 1), errorChan: make(chan *error, 1), signalChanTimeout: make(chan bool, 1)}
	defer close(pr.apiResponseChan)
	defer close(pr.errorChan)

	nl.pendingApiRequests <- pr

	select {

	case apiResponse := <-pr.apiResponseChan:
		// Got response
		return apiResponse.([]bool), nil

	case errorResponse := <-pr.errorChan:
		// Got error
		return nil, *errorResponse

	case <-time.After(nl.timeout):
		// Timeout
		pr.signalChanTimeout <- true
		return nil, errors.New("Receive timeout")
	}
}
